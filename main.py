#!/usr/bin/python3
# если вы решитесь запустить код, обратите внимаение, что вам нужно
# указать токен бота.

import signal
import sys
import time
import telebot
import string
import re
import sqlite3 as database
import configparser
from telebot import util
from telebot import types

# get_letters применяются для формы добавления записи (т.к. смайлики и 
# прочие символы не являются хорошими в официальных документах)
# get_links выделяет ссылки из текста 
get_letters = r'[0-9a-zA-Zа-яА-ЯёЁ!?;"^*()-., ]+'
get_links = r'(http|https)://[a-zA-Z0-9./?=_-]*'

# configs - это общий конфигурационный словарь,который держит в себе
# наборы переменных из bot.cfg
configs = {}

def updateCfg():
	global configs
	config_file = configparser.RawConfigParser()
	config_file.read("bot.cfg")
	configs['replicas'] = {}
	configs['admins'] = config_file['admins']['userid'].split(',')
	replicas = config_file['replicas']
	# вот это надо переделать. к сожалению configparser по какой-то
	# причине не умеет в абзацы. поэтому отметил их в конфигах как \n.
	# так низя! лучше переделать по красоте.
	for key in replicas:
		configs['replicas'][key] = replicas[key].replace('\\n', '\n')

# updateCfg сразу же пихает конфиги в требуемые словари
# словари перечислены в updateCfg: global |....|
#TODO: вынести в инициализацию __init__.py
updateCfg()

# подтягиваем в txt_msg конфиги ответов,
txt_msg = configs['replicas']
# а в restricted_users админов, которые могут делать рассылки.
restricted_users = configs['admins']

#TODO: переложить переменные в конфиг
# путь до базы данных и лога
dbPath = 'database.db'
logPath = 'bot.log'
# токен для телеги
token = '<TELEGRAM TOKEN>'
bot = telebot.TeleBot(token, threaded=False)


#подгружает базу в переменную (список с кортежами)
#TODO: разделить этот ужас как-нибудь
def initDB():
	global dbPath
	conn = database.connect(dbPath)
	cursor = conn.cursor()
	cursor.execute('SELECT userid, city, address, building_type, description FROM claims')
	result = cursor.fetchall()

	cursor.execute('SELECT userid, name, surname FROM users')
	users = cursor.fetchall()
	conn.close()

	userid_s = []
	for user in users:
		if user not in userid_s:
			userid_s.append(user[0])

	return result, userid_s

# инициализируем два кэша -- жалобы и пользователи
# userList используется для рассылки пользователям важных сообщений
# кроме того, в базе каждая запись закреплена за конкретным userid,
# что бы записи не были анонимны. таблица users хранит имя, фамилию и
# ник пользователя, который оставил сообщение. в планах добавить
# блокировку по userid спамеров (а их много) вангую, userList работает
# экстрахреново. TODO.
claimsCache, userList = initDB()

# добавляет запись в базу и в claimsCache
def addClaim(*ins):
	note = (ins[0],ins[1],ins[2],ins[3],ins[4])
	global claimsCache, dbPath
	claimsCache.append(note)

	conn = database.connect(dbPath)
	cursor = conn.cursor()
	cursor.execute("INSERT INTO claims VALUES ('{0}','{1}','{2}','{3}','{4}')".format(ins))
	conn.commit()
	conn.close()

# добавляет запись в базу и в userList
def addUser(message):
	values = (message.from_user.id,
               message.from_user.first_name,
               message.from_user.last_name,
               message.from_user.username)
	conn = database.connect(dbPath)
	cursor = conn.cursor()
	cursor.execute("INSERT INTO users VALUES ('{0}','{1}','{2}','{3}')".format(values))
	conn.commit()
	conn.close()

# оформляет список в красивенький удобочитаемый вид
def listClaims():
	global claimsCache
	out = ''
	for i in claimsCache:
		out += i[1] + ', ' + i[2] + ':\n' + 'тип:' + i[3] + '\n' + i[4] + '\n\n'
	return out


# filterClaims фильтрует все записи по указаному городу
def filterClaims():
	pass


# usr_dict нужен для хранения не до конца заполненых форм
# пользователями. используется в хэндлерах формы, и только.
usr_dict = {}
def sendClaims(userid):
	splitted_text = util.split_string('список всех жалоб:\n\n'+listClaims(), 3000)
	for text in splitted_text:
		keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
		keyboard.add(txt_msg['newclaimkey'])
		keyboard.add(txt_msg['listclaimskey'])
		bot.send_message(userid, text, reply_markup=keyboard)

# формочка для отправки сообщения об отмене
def sendReturn(userid):
	try:
		del usr_dict[message.from_user.id]
	except:
		pass
	keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
	keyboard.add(txt_msg['newclaimkey'])
	keyboard.add(txt_msg['listclaimskey'])
	bot.send_message(userid, txt_msg['cancelreply'], reply_markup=keyboard)


# следующие 4 функции -- форма для записи данных в базу
#
#***********************************************************************
def addr_add(message):
	print(str(message.from_user.id)+':'+message.text+'\n')
	if message.text == txt_msg['cancelkey'] or message.text == None:
		sendReturn(message.from_user.id)
	else:
		if re.fullmatch(only_letters, message.text):
			usr_dict[message.from_user.id] = [message.text]
			msg = bot.send_message(message.from_user.id, txt_msg['addrhint'])
			bot.register_next_step_handler(msg, build_add)
		else:
			msg = bot.send_message(message.from_user.id, txt_msg['notlettersreply'])
			bot.register_next_step_handler(msg, build_add)


def build_add(message):
	print(str(message.from_user.id)+':'+message.text+'\n')
	if message.text == txt_msg['cancelkey'] or message.text == None:
		sendReturn(message.from_user.id)
	else:
		if re.fullmatch(only_letters, message.text):
			usr_dict[message.from_user.id].append(message.text)
			msg = bot.send_message(message.from_user.id, txt_msg['buildtypehint'])
			bot.register_next_step_handler(msg, desc_add)
		else:
			msg = bot.send_message(message.from_user.id, txt_msg['notlettersreply'])
			bot.register_next_step_handler(msg, build_add)

def desc_add(message):
	print(str(message.from_user.id)+':'+message.text+'\n')
	if message.text == txt_msg['cancelkey'] or message.text == None:
		sendReturn(message.from_user.id)
	else:
		if re.fullmatch(only_letters, message.text):
			usr_dict[message.from_user.id].append(message.text)
			msg = bot.send_message(message.from_user.id, txt_msg['descriptionhint'])
			bot.register_next_step_handler(msg, end_add)
		else:
			msg = bot.send_message(message.from_user.id, txt_msg['notlettersreply'])
			bot.register_next_step_handler(msg, build_add)

def end_add(message):
	usr_dict[message.from_user.id].append(message.text)
	print(str(message.from_user.id)+':'+message.text+'\n')
	if message.text == txt_msg['cancelkey'] or message.text == None:
		sendReturn(message.from_user.id)
	else:
		if re.fullmatch(only_letters, message.text):
			keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
			keyboard.add(txt_msg['newclaimkey'])
			keyboard.add(txt_msg['listclaimskey'])
			addClaim(message.from_user.id, *usr_dict[message.from_user.id])
			bot.send_message(message.from_user.id, txt_msg['claimaddedreply'], reply_markup=keyboard)
		else:
			msg = bot.send_message(message.from_user.id, txt_msg['notlettersreply'])
			bot.register_next_step_handler(msg, build_add)
#***********************************************************************



# sendDispatch отсылает сообщения ВСЕМ ползователям, которые записаны в
# таблице users
#***********************************************************************
def sendDispatch(message):
	print(str(message.from_user.id)+':'+message.text+'\n')
	if message.text == txt_msg['cancelkey'] or message.text == None:
		sendReturn(message.from_user.id)

def sendToAll(message):
	for userid in userList:
		bot.send_message(userid, message.text)
#***********************************************************************


# log выводит сообщения на экран и сохраняет в файл (что бы можно было
# мониторить сообщения)
def log(text):
	print(text)
	print(text, file=(open(logPath, 'a')))

@bot.message_handler(content_types=['text'])
def on_text(message):
	userid = message.from_user.id
	print(userid, '\n'+message.text+'\n')
	if message.text == '/start':
		if userid in userList:
			pass
		else:
			addUser(message)
			userList.append(userid)
			print("New user detected, who hasn't used \"/start\" yet")

		keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
		keyboard.add(txt_msg['newclaimkey'])
		keyboard.add(txt_msg['listclaimskey'])
		bot.send_message(message.from_user.id, txt_msg['start'], reply_markup=keyboard)

	elif message.text == '/help':
		keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
		keyboard.add(txt_msg['newclaimkey'])
		keyboard.add(txt_msg['listclaimskey'])
		bot.send_message(userid, txt_msg['help'], reply_markup=keyboard)

	elif message.text == '/list' or message.text == '/spisok' or message.text == txt_msg['listclaimskey']:
		sendClaims(userid)

	#/newdoor или кнопка newcalimkey создает новую запись в базу
	elif message.text == '/newdoor' or message.text == txt_msg['newclaimkey']:
		keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
		keyboard.add(txt_msg['cancelkey'])
		msg = bot.send_message(userid, txt_msg['cityhint'], reply_markup=keyboard)
		bot.register_next_step_handler(msg, addr_add)

	#/send_dispatch рассылает всем известным userid сообщение.
	# используется только администраторами.
	elif message.text == '/send_dispatch':
		if message.from_user.id in restricted_users:
			msg = bot.send_message(userid, txt_msg['dispatch'], reply_markup=keyboard)
			bot.register_next_step_handler(msg, sendDispatch)
		else:
			bot.send_message(userid, 'Access denied')

	# поскольку обновление конфигов не нарушает работу бота ровным
	# счетом никак, обновлять их может любой. Возможно есть смысл
	# назначить пермишен по userid.
	elif message.text == '/update_configs':
		if message.from_user.id in restricted_users:
			global txt_msg, restricted_users
			updateCfg()
			txt_msg = configs['replicas']
			restricted_users = configs['admins']
			bot.send_message(userid, 'обновлено')
		else:
			bot.send_message(userid, 'Access denied')

	else:
		keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
		keyboard.add(txt_msg['newclaimkey'])
		keyboard.add(txt_msg['listclaimskey'])
		bot.send_message(userid, txt_msg['error'], reply_markup=keyboard)

#try\except сделан, что бы не допустить вылета из-за таймаута
# подключения или из-за того, что кто-то заблокировал бота
while True:
	try:
		bot.polling(none_stop=True)
	except Exception as err:
		print(str(err)+'\n')
		time.sleep(5)
		print("Internet error!\n", err)

#TODO: написать в конфиги userid, кому нужно отправить сообщение о ошибке
print('wow, program was stopped!')




